/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const blogs = /* GraphQL */ `
  query Blogs {
    blogs {
      id
      name
      posts {
        id
        title
        blogID
      }
    }
  }
`;
