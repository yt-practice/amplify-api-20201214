
const getBlogs = () => [{
    id: 12,
    name: 'hoge',
    posts: [],
}]

const resolvers = {
    Query: {
        blogs: _ctx => {
        return getBlogs();
      },
    },
    // Post: {
    //   comments: ctx => {
    //     return getCommentsForPost(ctx.source.id);
    //   },
    // },
  }
  
  // event
  // {
  //   "typeName": "Query", /* Filled dynamically based on @function usage location */
  //   "fieldName": "me", /* Filled dynamically based on @function usage location */
  //   "arguments": { /* GraphQL field arguments via $ctx.arguments */ },
  //   "identity": { /* AppSync identity object via $ctx.identity */ },
  //   "source": { /* The object returned by the parent resolver. E.G. if resolving field 'Post.comments', the source is the Post object. */ },
  //   "request": { /* AppSync request object. Contains things like headers. */ },
  //   "prev": { /* If using the built-in pipeline resolver support, this contains the object returned by the previous function. */ },
  // }
  exports.handler = async (event) => {
    const typeHandler = resolvers[event.typeName];
    if (typeHandler) {
      const resolver = typeHandler[event.fieldName];
      if (resolver) {
        return await resolver(event);
      }
    }
    throw new Error("Resolver not found.");
  };
  

// exports.handler = async (_event) => {
//     // TODO implement
//     const response = {
//         statusCode: 200,
//     //  Uncomment below to enable CORS requests
//         headers: {
//             "Access-Control-Allow-Origin": "*",
//             "Access-Control-Allow-Headers": "*"
//         }, 
//         body: JSON.stringify('Hello from Lambda!'),
//     };
//     return response;
// };
